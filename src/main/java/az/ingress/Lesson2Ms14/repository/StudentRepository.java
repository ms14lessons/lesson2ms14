package az.ingress.Lesson2Ms14.repository;

import az.ingress.Lesson2Ms14.controller.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student,Integer> {

}
