package az.ingress.Lesson2Ms14;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson2Ms14Application {

	public static void main(String[] args) {
		SpringApplication.run(Lesson2Ms14Application.class, args);
	}

}
