package az.ingress.Lesson2Ms14.controller;

import az.ingress.Lesson2Ms14.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student")
public class StudentController {
    private final StudentService studentService;
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping
    public void create(@RequestBody Student student) {
        studentService.create(student);

    }

    @PutMapping
    public void update(@RequestBody Student student) {
        studentService.update(student);

    }

    @GetMapping("/{id}")
    public Student get(@PathVariable Integer id) {
        return studentService.get(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        studentService.delete(id);
    }


}
