package az.ingress.Lesson2Ms14.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ms14")
public class GreetingController {
    @GetMapping
    public String sayHello(@RequestParam String lang) {
        switch (lang) {
            case "en":
                return "Hello from Ms14";
            case "az":
                return "Ms14-den salam";
            case "ru":
                return "Privet iz ms14";
            default:
                return "i dont understand";
        }
    }

    @PostMapping
    public String create(@RequestBody Student student) {
        System.out.println("Student name is " + student.getName());
        System.out.println("Student surname is " + student.getSurname());
        System.out.println("Student id is " + student.getId());
        return "Hello, " + student.getName();
    }


    @PutMapping
    public void updateStudent(@RequestBody Student student) {

        System.out.println("Student updated");

    }

    @DeleteMapping("/{id}")
    public String deleteStudent(@PathVariable("id") Integer studentID) {
        return "Student with id" + studentID + " is deleted";
    }

    @PostMapping("/register")
    public void register(@RequestBody Register register) {
        if (!register.getPassword().equals(register.getRepeatPassword())) {
            throw new RuntimeException();
        }
        System.out.println("User with email " + register.getEmail() + " successfully created");
    }

}
