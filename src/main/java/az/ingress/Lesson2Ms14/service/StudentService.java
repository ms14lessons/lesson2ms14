package az.ingress.Lesson2Ms14.service;

import az.ingress.Lesson2Ms14.controller.Student;
import az.ingress.Lesson2Ms14.repository.StudentRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

    private final StudentRepository studentRepository;

    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public void create(Student student) {
        studentRepository.save(student);

    }

    public void update(Student student) {
        studentRepository.save(student);
    }

    public Student get(Integer id) {
        return studentRepository.findById(id).get();
    }

    public void delete(Integer id) {
        studentRepository.deleteById(id);
    }
}
